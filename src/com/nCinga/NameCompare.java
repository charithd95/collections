package com.nCinga;

import java.util.Comparator;

public abstract class NameCompare implements Comparable {
    public int compare(Movie m1, Movie m2)
    {
        return m1.getName().compareTo(m2.getName());
    }
}
