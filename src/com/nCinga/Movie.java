package com.nCinga;

public class Movie implements Comparable<Movie>{
    private String name;
    private Double rating;
    private Integer releaseYear;

    public Movie(String name, Double rating, Integer releaseYear) {
        this.name = name;
        this.rating = rating;
        this.releaseYear = releaseYear;
    }
    public int compareTo(Movie movie){
        return name.compareTo(movie.name);
    }
    public String toString(){
        return "Movie{"+"name="+name+",Rating="+rating+",Release Year="+releaseYear+"}";
    }




    public String getName() {
        return name;
    }

    public Double getRating() {
        return rating;
    }

    public Integer getReleaseYear() {
        return releaseYear;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public void setReleaseYear(Integer releaseYear) {
        this.releaseYear = releaseYear;
    }
}
