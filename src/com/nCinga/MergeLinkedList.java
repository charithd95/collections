package com.nCinga;

import java.util.LinkedList;

public class MergeLinkedList {
    public static void main(String[] args) {
        LinkedList<Integer> linkedListOne = new LinkedList<Integer>();
        LinkedList<Integer> linkedListTwo = new LinkedList<Integer>();
        LinkedList<Integer> linkedListOutput = new LinkedList<Integer>();
        linkedListOne.add(1);
        linkedListOne.add(5);
        linkedListOne.add(6);
        linkedListOne.add(7);
        linkedListTwo.add(2);
        linkedListTwo.add(3);
        linkedListTwo.add(8);
        linkedListTwo.add(4);
        linkedListOne.sort(Integer::compareTo);
        linkedListTwo.sort(Integer::compareTo);
        int j=0;
        int k=0;
        for (int i = 0; i <linkedListOne.size()+linkedListTwo.size(); i++) {
            if (j<linkedListOne.size()&&(k<linkedListTwo.size())){
                if(linkedListOne.get(j)-linkedListTwo.get(k)<0){
                    linkedListOutput.add(linkedListOne.get(j));
                    j++;
                }
                else{
                    linkedListOutput.add(linkedListTwo.get(k));
                    k++;
                }
            }
            else if (j<linkedListOne.size()){
                linkedListOutput.add(linkedListOne.get(j));
                j++;
            }
            else if (k<linkedListTwo.size()){
                linkedListOutput.add(linkedListTwo.get(k));
                k++;
            }
        }
        System.out.println(linkedListOutput);
    }
}

