package com.nCinga;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

public class SortByFrequency {
    public static void main(String[] args) {
        LinkedList<Integer> input = new LinkedList<Integer>();
        input.add(1);
        input.add(5);
        input.add(3);
        input.add(6);
        input.add(5);
        input.add(2);
        input.add(3);
        input.add(7);
        input.add(3);
        input.add(8);
        input.add(3);
        System.out.println(sort(input));
    }
    public static LinkedList<Integer> sort(LinkedList<Integer> input){
        LinkedHashMap<Integer,Integer> linkedHashMap =new LinkedHashMap<Integer,Integer>();
        for (Integer integer:input){
            if (linkedHashMap.get(integer)==null){
                linkedHashMap.put(integer ,1);
            }
            else{
                int count = linkedHashMap.get(integer)+1;
                linkedHashMap.replace(integer,count);
            }
        }
        LinkedList<Integer> output = new LinkedList<Integer>();
        for(Map.Entry<Integer,Integer> entry:linkedHashMap.entrySet()){
            if (entry.getValue()==1){
                output.add(entry.getKey());
            }
            else{
                for (int index = 0; index <entry.getValue() ; index++) {
                    output.addFirst(entry.getKey());
                }
            }

        }
        return output;
    }

}
