package com.nCinga;

import com.nCinga.lamda.Employee;

import java.util.*;
import java.util.function.Predicate;

public class Main {

    public static <ListNode> void main(String[] args) {
	// write your code her
//        ArrayList<Movie> movies = new ArrayList<Movie>();
//        movies.add(new Movie("Lone survivor",8.9,2005));
//        movies.add(new Movie("Hurt Locker",7.9,2006));
//        movies.add(new Movie("Hangover",9.0,2009));
//        RatingCompare ratingCompare = new RatingCompare();
//        Collections.sort(movies, ratingCompare);
//        System.out.println(movies);
//        Collections.sort(movies, new ReleaseYearComparator());
//        System.out.println(movies);
//        movies.sort(Comparator.comparing(Movie::toString));
//        System.out.println(movies);
        Optional<Employee> empOne=Optional.empty();
        Optional<Employee> empTwo = Optional.of(new Employee(500000, "Sun"));
        Predicate<Employee> predicate=x->(x.getName().startsWith("S"));
        System.out.println(empTwo.filter(predicate));
//        System.out.println(empOne.orElse(new Employee(500,"Alice")));
//        System.out.println(empOne.orElseThrow(IllegalStateException::new));



}
}
