package com.nCinga;

import java.util.Comparator;

public class ReleaseYearComparator implements Comparator<Movie> {
    public int compare(Movie movie,Movie movieOne){
        return movie.getReleaseYear()-movieOne.getReleaseYear();
    }
}
