package com.nCinga.lamda;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Stream;

public class Streams {
    public static void main(String[] args) {
        Stream<Integer> stream = Stream.of(1,2,3,4,5,6,7,8,9);
        Optional<Integer> out= stream.reduce((acc, item) -> item + acc);
        //System.out.println(out);
        Integer num = Integer.valueOf(out.get());
        System.out.println(num);
        Stream<Integer> streamOne = Stream.of(1,2,3,4,5,6,7,8,9);
        ArrayList<Integer> arrayList=new ArrayList<>();
        streamOne.sorted(Collections.reverseOrder()).forEach(x->arrayList.add(x));
        System.out.println(arrayList);

    }
}
