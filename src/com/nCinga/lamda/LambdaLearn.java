package com.nCinga.lamda;

import jdk.dynalink.linker.LinkerServices;

import java.util.*;

public class LambdaLearn {
    interface even{
        boolean checkNumber(int number);
    }

    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(4);
        arrayList.add(5);
        arrayList.add(6);
        arrayList.add(7);
        arrayList.add(8);
        even e=x->x%2==0;
        for (int i:arrayList) {
            if (e.checkNumber(i)==true){
                System.out.println(i+" ");
            }

        }
    }




}
