package com.nCinga.lamda;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.function.Consumer;

import static java.lang.StrictMath.abs;

public class Palindrome {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("sam", "bob", "sim","level");
        Consumer<String> palindrome = a -> {
            StringBuilder stringBuilder = new StringBuilder(a);
            String reversed= stringBuilder.reverse().toString();
            System.out.println(a);
            System.out.println(reversed);

            if (a.equals(reversed)){
                System.out.println("This is a palindrome");
            }
            else{
                System.out.println("This is not a palindrome");
            }
        };
        names.forEach(palindrome);
    }
}
