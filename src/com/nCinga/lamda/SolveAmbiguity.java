package com.nCinga.lamda;

public class SolveAmbiguity {
    public interface A {
        default void printName(){
            System.out.println("from A");
        }
    }

    public interface B {
        default void printName() {
            System.out.println("from B");
        }
    }

    public static class D implements B, A {
        public static void main(String[] args) {
            new D().printName();
        }

        public void printName() {
            B.super.printName();
        }
    }
}
